#ifndef PAINTCOST_H_INCLUDED
#define PAINTCOST_H_INCLUDED

class PaintCost {
public:
    PaintCost();
    int getCoast(int area);
};

#endif // PAINTCOST_H_INCLUDED
