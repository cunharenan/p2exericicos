#include <iostream>
#include "Rectangle.h"
using namespace std;

Rectangle::Rectangle(int w, int h):Shape(w, h):PaintCost() {
};

int Rectangle::getArea() const {
    return getWidth() * getHeight();
}
