#ifndef RECTANGLE_H_INCLUDED
#define RECTANGLE_H_INCLUDED

#include <iostream>
#include <string>
#include "Shape.h"
using namespace std;

class Rectangle : public Shape, public PaintCost
{

public:
    Rectangle(int w, int h);
    int getArea() const;
    friend void printRectangle(Rectangle rec);
};

#endif // RECTANGLE_H_INCLUDED
