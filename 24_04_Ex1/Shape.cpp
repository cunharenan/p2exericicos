#include <iostream>
#include "Shape.h"
using namespace std;

Shape::Shape(int w, int h) {
    setWidth(w);
    setHeight(h);
}

void Shape::setWidth(int w) {
    width = w;
}

void Shape::setHeight(int h) {
    height = h;
}

int Shape::getHeight() const {
    return height;
}

int Shape::getWidth() const {
    return width;
}
