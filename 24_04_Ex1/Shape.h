#ifndef SHAPE_H_INCLUDED
#define SHAPE_H_INCLUDED

#include <iostream>
#include <string>
using namespace std;

class Shape {

protected:

private:
    int width;
    int height;

public:
    Shape(int width, int height);
    void setWidth(int w);
    void setHeight(int h);
    int getWidth() const;
    int getHeight() const;
    friend void printShape(Shape shape);
    friend void printAll(Shape shape);

};


#endif // SHAPE_H_INCLUDED
