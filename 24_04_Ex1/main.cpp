#include <iostream>
#include "Rectangle.h"
using namespace std;

void printShape(Shape shape) {
    cout << shape.getWidth() << " " << shape.getHeight();
}

void printAll(Shape shape) {
    cout << shape.getWidth() << " " << shape.getHeight();
}

void printRectangle(Rectangle rec) {
    cout << rec.getWidth() << " " << rec.getHeight();
}

int main()
{

    Rectangle rec(10, 20);

    cout << rec.getArea() << endl;

    printShape(rec);
    printAll(rec);
    printRectangle(rec);

    return 0;
}
