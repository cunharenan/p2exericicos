#include <iostream>

using namespace std;

inline int Max(int x, int y)
{
    return (x > y) ? x : y;
}

void print(int x)
{
    cout << "Print 1:" << endl;
    cout << x << endl;
    cout << "\n";
}

void print(int x, int y)
{
    cout << "Print 2:" << endl;
    cout << x << endl;
    cout << y << endl;
    cout << "\n";
}

void print(int x, int y, int z)
{
    cout << "Print 3:" << endl;
    cout << x << endl;
    cout << y << endl;
    cout << z << endl;
    cout << "\n";
}

class Point
{

public:
    int x, y;
    Point(int a, int b)
    {
       x = a;
       y = b;
    }
    ~Point();
    Point operator+(Point p);
    static void print(Point p);
};

Point::~Point()
{

}

// sobrecarga de operadores
Point Point::operator+(Point p)
{
    int a, b;
    a = x + p.x;
    b = y + p.y;
    return Point(a, b);
}

void Point::print(Point p)
{
    cout << "P3: (" << p.x << ", " << p.y << ")";
}

int main()
{
    int* pvalue = NULL;
    pvalue = new int;

    *pvalue = 10;
    for(int i = 1; i <= 10; i++)
    {
        pvalue = new int;
        cout << pvalue << endl;

        //delete pvalue; //tira o pvalue da memoria
    }

    cout << Max(10, 20) << endl;

    print(10);
    print(10, 20);
    print(10, 20, 30);

    cout << "Somando pontos: " << endl;
    Point p1(10, 50);
    Point p2(20, 30);
    Point p3 = p1 + p2;
    Point::print(p3);

    return 0;
}
