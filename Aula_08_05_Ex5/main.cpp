#include <iostream>

using namespace std;

class Box
{

public:
    Box(double l = 0.0, double w = 0.0, double h = 0.0)
    {
        cout << "Constructor called." << endl;
        setHeight( h );
        setWidth( w );
        setLength( l );
    }
    void setHeight(int h)
    {
        height = h;
    }
    void setWidth(int w)
    {
        width = w;
    }
    void setLength(int l)
    {
        length = l;
    }
    double getVolume()
    {
        return length * width * height;
    }
    int compare(Box box)
    {
        if(this->getVolume() > box.getVolume())
            return 1;
        else
            return 0;
    }
    friend void printBox(Box box);

protected:
    int height;
    int width;
    int length;
};

void printBox(Box box)
{
    cout << "Print box:" << endl;
    cout << "W: " << box.width << endl;
    cout << "H: " << box.height << endl;
    cout << "L: " << box.length << endl;
    cout << "V: " << box.getVolume() << endl;
}

int main()
{
    /*
    Box b1(2, 2, 2);
    Box b2(3, 3, 3);

    printBox(b1);

    if(b1.compare(b2))
        cout << "Box 2 is smaller than Box 1";
    else
        cout << "Box 1 is smaller than Box 1";
        */

    // Exemplo 3 - aloca��o dinamica
    Box* b3 = new Box(2, 2, 2);

    return 0;
}
