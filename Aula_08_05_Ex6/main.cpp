#include <iostream>
#include <string>

using namespace std;

class Shape
{
public:
    Shape(int w = 0, int h = 0, string c = "blue")
    {
        cout << "Shape constructor called." << endl;
        setWidth(w);
        setHeight(h);
        setColor(c);
    }
    void setColor(string c)
    {
        color = c;
    }
    void setWidth(int w)
    {
        while(w < 0) {
            cout << "Invalid width! Enter again:" << endl;
            cin >> w;
        }
        width = w;
    }
    void setHeight(int h)
    {
        while(h < 0) {
            cout << "Invalid height! Enter again:" << endl;
            cin >> h;
        }
        height = h;
    }
    int getWidth()
    {
        return width;
    }
    int getHeight()
    {
        return height;
    }
    string getColor()
    {
        return color;
    }
    virtual int getArea() = 0;
    virtual void print() = 0;
    void printShape()
    {
        cout << "\n Parent class PrintShape" << endl;
        cout << "width: " << getWidth() << endl;
        cout << "height: " << getHeight() << endl;
    }

protected:
    int height;
    int width;
    string color;
};

class Rectangle : public Shape
{
public:
    Rectangle(int w = 0, int h = 0, string c = "blue"): Shape(w, h, c) {
        setWidth(w);
        setHeight(h);
    }
    virtual int getArea()
    {
        cout << "Rectangle class area: " << endl;
        return (getWidth() * getHeight());
    }
    virtual void print()
    {
        cout << "\nRectangle:" << endl;
        cout << "W: " << getWidth() << endl;
        cout << "H: " << getHeight() << endl;
        cout << "Color: " << getColor() << endl;
        cout << "Area: " << getArea() << endl;
    }
};

class Square : public Shape
{
public:
    Square(int w = 0, int h = 0, string c = "blue"): Shape(w, h, c) {
        setWidth(w);
        setHeight(h);
    }
    virtual int getArea()
    {
        cout << "Square class area: " << endl;
        return (getWidth() * getHeight());
    }
    virtual void print()
    {
        cout << "\nSquare:" << endl;
        cout << "W: " << getWidth() << endl;
        cout << "H: " << getHeight() << endl;
        cout << "Color: " << getColor() << endl;
        cout << "Area: " << getArea() << endl;
    }
};

class Circle : public Shape
{
public:
    Circle(int r = 0, string c = "blue"): Shape(0, 0, c) {
        setWidth(0);
        setHeight(0);
        setRadius(r);
    }
    void setRadius(int r)
    {
        while(r < 0) {
            cout << "Invalid radius! Enter again:" << endl;
            cin >> r;
        }
        radius = r;
    }
    int getRadius()
    {
        return radius;
    }
    virtual int getArea()
    {
        cout << "Circle class area: " << endl;
        return (3.14*getRadius()*getRadius());
    }
    virtual void print()
    {
        cout << "\nCircle:" << endl;
        cout << "R: " << getRadius() << endl;
        cout << "Color: " << getColor() << endl;
        cout << "Area: " << getArea() << endl;
    }
protected:
    int radius;
};

class Triangle : public Shape
{
public:
    Triangle(int w = 0, int h = 0, string c = "blue"): Shape(w, h, c) {
        setWidth(w);
        setHeight(h);
    }
    virtual int getArea()
    {
        cout << "Triangle class area: " << endl;
        return ((getWidth() * getHeight())/2);
    }
    virtual void print()
    {
        cout << "\nTriangle:" << endl;
        cout << "Base: " << getWidth() << endl;
        cout << "H: " << getHeight() << endl;
        cout << "Color: " << getColor() << endl;
        cout << "Area: " << getArea() << endl;
    }
};

int main()
{
    Shape *shape;
    int a;
    Rectangle rec(10, 7, "red");
    Triangle tri(10, 5, "red");

    shape = &rec;

    a = shape->getArea();
    cout << "\n\n 1)________________________";
    cout << "\n shape = &rec";
    cout << "\n shape.getArea()\n\n";
    cout << "\nArea = " << a << endl;

    cout << "\n\n 2)________________________";
    cout << "\n rec.getArea()\n\n";
    a = rec.getArea();
    cout << "\nArea = " << a << endl;

    cout << "\n\n 3)_________________________";
    cout << "\n shape = &tri \n";
    shape = &tri;
    a = shape->getArea();
    cout << "\nArea = " << a << endl;

    cout << "\n\n 4)________________________";
    cout << "\n tri.getArea()\n\n";
    a = tri.getArea();
    cout << "\nArea = " << a << endl;

    cout << "\n\nTest function print:\n" << endl;

    /* Triangulo */
    tri.print();

    /* Retangulo */
    rec.print();

    /* Circulo */
    Circle cic(10, "red");
    cic.print();

    return 0;
}
