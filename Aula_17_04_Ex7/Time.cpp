#include <iostream>
#include <iomanip>

#include "Time.h"
using namespace std;

Time::Time(int h, int m, int s) {
    setHour(h);
    setMinute(m);
    setSecond(s);
}

int Time::getHour() {
    return hour;
}

int Time::getMinute() {
    return minute;
}

int Time::getSecond() {
    return second;
}

void Time::setHour(int h) {
    hour = h;
}

void Time::setMinute(int m) {
    minute = m;
}

void Time::setSecond(int s) {
    second = s;
}

void Time::setTime(int h, int m, int s) {
    setHour(h);
    setMinute(m);
    setSecond(s);
}

void Time::print()
{
    cout << setfill('0');
    cout << setw(2) << hour
         << ":" << setw(2) << minute
         << ":" << setw(2) << second << endl;
}

void Time::nextSecond() {
    ++second;
    if(second >= 60) {
        second = 0;
        ++minute;
    }
    if(minute >= 60) {
        minute = 0;
        ++hour;
    }
    if(hour >= 24) {
        hour = 0;
    }
}

void Time::previousSecond() {
    --second;
    if(second < 0) {
        second = 59;
        --minute;
    }
    if(minute < 0) {
        minute = 59;
        --hour;
    }
    if(hour < 0) {
        hour = 23;
    }
}

void Time::previousMinute() {
    --minute;
    if(minute < 0) {
        minute = 59;
        --hour;
    }
    if(hour < 0) {
        hour = 23;
    }
}

void Time::nextMinute() {
    ++minute;
    if(minute >= 60) {
        minute = 0;
        ++hour;
    }
    if(hour >= 24) {
        hour = 0;
    }
}

void Time::nextHour() {
    ++hour;
    if(hour >= 24) {
        hour = 0;
    }
}

void Time::previousHour() {
    --hour;
    if(hour < 0) {
        hour = 23;
    }
}

void Time::printAmPm() {
    int tmp_hour = hour >= 12 ? hour-12 : hour;
    string t = hour >= 12 ? "PM" : "AM";

    cout << setfill('0');
    cout << setw(2) << tmp_hour
         << ":" << setw(2) << minute
         << ":" << setw(2) << second
         << " " << t << endl;

}

