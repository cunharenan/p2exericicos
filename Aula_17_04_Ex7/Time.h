#ifndef TIME_H_INCLUDED
#define TIME_H_INCLUDED

#include <iostream>
#include <string>
using namespace std;

class Time
{
private:
    int hour;
    int minute;
    int second;

public:
    Time(int h, int m, int s);
    int getHour();
    int getMinute();
    int getSecond();
    void setHour(int h);
    void setMinute(int m);
    void setSecond(int s);
    void setTime(int h, int m, int s);
    void print();
    void nextSecond();
    void previousSecond();
    void nextMinute();
    void previousMinute();
    void nextHour();
    void previousHour();
    void printAmPm();
};

#endif // TIME_H_INCLUDED
