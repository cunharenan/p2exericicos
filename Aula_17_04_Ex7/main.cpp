#include <iostream>
#include "Time.h"

using namespace std;

int main()
{

    Time t1(23, 59, 59);
    cout << "t1: estado inicial: ";
    t1.print();

    t1.setHour(12);
    t1.setMinute(30);
    t1.setSecond(15);
    cout << "t1: set valores: ";
    t1.print();

    cout << "Object 1 state: ";
    cout << "Hour is: " << t1.getHour() << endl;
    cout << "Minute is: " << t1.getMinute() << endl;
    cout << "Second is: " << t1.getSecond() << endl;

    t1.printAmPm();

    return 0;
}
